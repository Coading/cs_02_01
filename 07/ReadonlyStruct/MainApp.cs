﻿using System;
using System.Collections.Generic;

namespace ReadonlyStruct
{
    readonly struct RGBColr
    {
        public readonly byte R;
        public readonly byte G;
        public readonly byte B;

        public RGBColr(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }
    }

    class MainApp
    {
        static void Main(string[] args)
        {
            RGBColr Red = new RGBColr(255, 0, 0);
            //Red.G = 100; // 컴파일 에러
        }
    }
}